## Requirements

**Install:**
* [Docker](https://docs.docker.com/engine/install/)
* [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
* [skaffold](https://skaffold.dev/docs/install/)

## Usage

1. Create a cluster:
```
$ kind create cluster --config ./environment/kind.yaml
```

2. Run skaffold to start your own Continuous Deployment:
```
$ cd environment 
$ skaffold dev
```

3. Open http://127.0.0.1:81/ or http://127.0.0.1:82/
